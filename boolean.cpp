 #include "boolean.hpp"

  namespace animalfarm{
 const char * const BoolToString(bool b)
{
  return b ? "true" : "false";
}
  }